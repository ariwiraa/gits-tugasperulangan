package main

import "fmt"

func main() {
	var num, subMenu int
	fmt.Println("=== Jenis Segitiga ===")
	fmt.Println("1.Segitiga\n2.Segitiga Terbalik")
	fmt.Print("Pilih menu sesuai dengan angka : ")
	fmt.Scan(&subMenu)

	if subMenu == 1 {
		fmt.Print("Masukan jumlah baris : ")
		fmt.Scan(&num)
		for i := 1; i <= num; i++ {
			for j := num; j > i; j-- {
				fmt.Print(" ")
			}

			for k := 1; k <= i; k++ {
				fmt.Print("*")
			}

			for l := 1; l <= i-1; l++ {
				fmt.Print("*")
			}

			fmt.Println("")
		}
	} else if subMenu == 2 {
		fmt.Print("Masukan jumlah baris : ")
		fmt.Scan(&num)
		for i := num; i >= 1; i-- {
			for j := num; j > i; j-- {
				fmt.Print(" ")
			}

			for k := 1; k <= i; k++ {
				fmt.Print("*")
			}

			for l := 1; l <= i-1; l++ {
				fmt.Print("*")
			}

			fmt.Println("")
		}

	} else {
		fmt.Println("Pilihan tidak ada")
	}
}
